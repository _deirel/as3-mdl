/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.impl {
import flash.display.DisplayObject;

import ru.deirel.mdl.core.ClassMap;

import starling.display.DisplayObject;

public class DisplayContent extends ClassMap {
    public function DisplayContent() {
        super(flash.display.DisplayObject, starling.display.DisplayObject);
    }
}
}
