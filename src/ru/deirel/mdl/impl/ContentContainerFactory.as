/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.impl {
import flash.display.DisplayObject;

import ru.deirel.mdl.api.IContentContainer;

import starling.display.DisplayObject;

public class ContentContainerFactory {
    static public function get(container:*):IContentContainer {
        if (container is flash.display.DisplayObject) {
            return new FlashContentContainer(container);
        } else if (container is starling.display.DisplayObject) {
            return new StarlingContentContainer(container);
        }
        return null;
    }
}
}
