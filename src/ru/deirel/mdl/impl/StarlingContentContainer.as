/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.impl {
import ru.deirel.mdl.api.IContentContainer;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

public class StarlingContentContainer implements IContentContainer {
    private var _base:DisplayObjectContainer;

    public function StarlingContentContainer(base:DisplayObjectContainer) {
        _base = base;
    }

    public function addContent(vec:*):void {
        var typedVec:Vector.<DisplayObject> = vec as Vector.<DisplayObject>;
        if (typedVec) {
            for (var i:int = 0, l:int = typedVec.length; i < l; i++) {
                _base.addChild(typedVec[i]);
            }
        }
    }

    public function removeContent(vec:*):void {
        var typedVec:Vector.<DisplayObject> = vec as Vector.<DisplayObject>;
        if (typedVec) {
            for (var i:int = 0, l:int = typedVec.length; i < l; i++) {
                var disp:DisplayObject = typedVec[i];
                if (disp.parent == _base) {
                    _base.removeChild(disp);
                }
            }
        }
    }
}
}
