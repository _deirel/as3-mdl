/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.impl {
import flash.utils.Dictionary;

import ru.deirel.mdl.api.ContainerDef;
import ru.deirel.mdl.api.IContentConfig;
import ru.deirel.mdl.api.IContentContainer;
import ru.deirel.mdl.core.ClassMap;

public class ContentManager {
    /// BaseClass:Class => IContentContainer
    private var _containersMap:Dictionary = new Dictionary();

    public function ContentManager(config:IContentConfig) {
        var containers:Vector.<ContainerDef> = config.getContainers();
        for (var i:int = 0; i < containers.length; i++) {
            registerContentContainer(containers[i]);
        }
    }

    private function registerContentContainer(def:ContainerDef):void {
        var contentContainer:IContentContainer = ContentContainerFactory.get(def.container);
        if (contentContainer) {
            _containersMap[def.baseClass] = contentContainer;
        }
    }

    public function addContent(content:ClassMap):void {
        processContent(content, true);
    }

    public function removeContent(content:ClassMap):void {
        processContent(content, false);
    }

    private function processContent(content:ClassMap, add:Boolean):void {
        var keys:Vector.<Class> = content.getKeys();
        for each (var key:Class in keys) {
            var contentContainer:IContentContainer = _containersMap[key];
            if (contentContainer) {
                if (add) {
                    contentContainer.addContent(content.grab(key));
                } else {
                    contentContainer.removeContent(content.grab(key));
                }
            }
        }
    }
}
}
