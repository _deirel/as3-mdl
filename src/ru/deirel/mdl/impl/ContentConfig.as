/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.impl {

import flash.display.DisplayObjectContainer;

import ru.deirel.mdl.api.ContainerDef;
import ru.deirel.mdl.api.IContentConfig;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

public class ContentConfig implements IContentConfig {
    private var _vec:Vector.<ContainerDef>;

    public function ContentConfig(flashContainer:flash.display.DisplayObjectContainer, starlingContainer:starling.display.DisplayObjectContainer) {
        _vec = new Vector.<ContainerDef>();
        if (flashContainer) {
            _vec.push(new ContainerDef(flash.display.DisplayObject, flashContainer));
        }
        if (starlingContainer) {
            _vec.push(new ContainerDef(DisplayObject, starlingContainer));
        }
    }

    public function getContainers():Vector.<ContainerDef> {
        return _vec;
    }
}
}
