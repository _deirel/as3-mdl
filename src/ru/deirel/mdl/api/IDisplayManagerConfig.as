/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.api {
public interface IDisplayManagerConfig {
    function getContentConfig(type:int):IContentConfig;
}
}
