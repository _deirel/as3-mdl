/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.api {
public class ContainerDef {
    public var baseClass:Class;
    public var container:*;

    public function ContainerDef(baseClass:Class, container:*) {
        this.baseClass = baseClass;
        this.container = container;
    }
}
}
