/**
 * Created by dev on 15.09.2016.
 */
package ru.deirel.mdl.api {
public interface IContentContainer {
    function addContent(vec:*):void;
    function removeContent(vec:*):void;
}
}
