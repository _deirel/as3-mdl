package ru.deirel.mdl.core {
	/**
	 * ...
	 * @author 
	 */
	public class ClassMapData {
		public var cls:Class;
		private var _vec:*;
		public var length:int;
		
		public function ClassMapData(cls:Class) {
			this.cls = cls;
			_vec = Reflect.createVectorOfClass(cls);
		}
		
		public function push(obj:*):void {
			length = _vec.push(obj);
		}
		
		public function get vec():* {
			return _vec;
		}
	}
}