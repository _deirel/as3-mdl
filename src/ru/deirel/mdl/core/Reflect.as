package ru.deirel.mdl.core {
	import avmplus.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author 
	 */
	public class Reflect {
		static public function createVectorOfClass(cls:Class):* {
			var className:String = getQualifiedClassName(cls);
			var vectorClassName:String = "__AS3__.vec::Vector.<" + className + ">";
			var vectorClass:Class = getDefinitionByName(vectorClassName) as Class;
			return new vectorClass();
		}
	}

}