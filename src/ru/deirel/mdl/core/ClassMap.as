package ru.deirel.mdl.core {
	/**
	 * ... 3700 + 
	 * @author 
	 */
	public class ClassMap {
		private var _data:Vector.<ClassMapData> = new Vector.<ClassMapData>();
		private var _keys:Vector.<Class> = new Vector.<Class>();
		private var _numClasses:int = 0;
		
		public function ClassMap(cls1:Class, ...rest) {
			addConstraint(cls1);
			for (var i:int = 0, l:int = rest.length; i < l; i++) {
				addConstraint(rest[i]);
			}
		}
		
		private function addConstraint(cls:Class):void {
			_numClasses = _data.push(new ClassMapData(cls));
            _keys.push(cls);
		}
		
		public function add(value:*):void {
			for (var i:int = 0; i < _numClasses; i++) {
				var data:ClassMapData = _data[i];
				if (value is data.cls) {
					data.push(value);
					return;
				}
			}
			throw new ArgumentError("Constraints are not satisfied");
		}
		
		public function grab(cls:Class):* {
			for (var i:int = 0; i < _numClasses; i++) {
				var data:ClassMapData = _data[i];
				if (data.cls == cls) return data.vec;
			}
			throw new ArgumentError("Constraints are not satisfied");
		}

        public function getKeys():Vector.<Class> {
            return _keys.concat();
        }
		
		public function destroy(destroyElements:Boolean = false, destroyMethod:String = "destroy"):void {
			if (destroyElements) {
				for (var i:int = 0; i < _numClasses; i++) {
					var data:ClassMapData = _data[i];
					for (var j:int = 0; j < data.length; j++) {
						var obj:Object = data.vec[j];
						var destroyFunc:Function = obj.hasOwnProperty(destroyMethod) ? obj[destroyMethod] as Function : null;
						if (destroyFunc) destroyFunc();
					}
				}
			}
			_data = null;
		}
	}
}