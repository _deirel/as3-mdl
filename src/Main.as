package{
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

import ru.deirel.mdl.impl.DisplayContent;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.events.Event;

import test.DisplayManagerConfigTest;
import test.DisplayManagerTest;

/**
	 * ...
	 * @author 
	 */
	public class Main extends Sprite {

		private var _starling:Starling;

		public function Main() {
			if (stage) init(); else addEventListener(flash.events.Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:flash.events.Event = null):void {
			removeEventListener(flash.events.Event.ADDED_TO_STAGE, init);

			_starling = new Starling(App, stage);
			_starling.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
			_starling.start();
		}

		private function onRootCreated(e:starling.events.Event):void {
			var config:DisplayManagerConfigTest = new DisplayManagerConfigTest();
			stage.addChild(config.flashBase);
			_starling.root.stage.addChild(config.starlingBase);

			var manager:DisplayManagerTest = new DisplayManagerTest(config);

			var contentBg:DisplayContent = new DisplayContent();
			contentBg.add(createFlashSprite(30, 0xff0000));
			contentBg.add(createStarlingSprite(10, 0x0000ff));

			var contentFg:DisplayContent = new DisplayContent();
			contentFg.add(createFlashSprite(40, 0xff8080));
			contentFg.add(createStarlingSprite(20, 0x8080ff));

			manager.background.addContent(contentBg);
			manager.foreground.addContent(contentFg);

			var n:int = 0;
			stage.addEventListener(MouseEvent.CLICK, function (e:MouseEvent):void {
				n++;
				if (n == 1) manager.background.removeContent(contentBg);
				else if (n == 2) manager.foreground.removeContent(contentFg);
			});
		}

		private function createFlashSprite(offset:Number, color:uint):Sprite {
			var spr:Sprite = new Sprite();
			spr.graphics.beginFill(color);
			spr.graphics.drawRect(0, 0, 50, 50);
			spr.x = spr.y = offset;
			return spr;
		}

		private function createStarlingSprite(offset:Number, color:uint):DisplayObject {
			var quad:Quad = new Quad(100, 100, color);
			quad.x = quad.y = offset;
			return quad;
		}
	}
	
}

import starling.display.Sprite;

class App extends Sprite {
}