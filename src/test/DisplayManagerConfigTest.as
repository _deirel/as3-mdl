/**
 * Created by dev on 15.09.2016.
 */
package test {
import flash.display.Sprite;

import ru.deirel.mdl.api.IContentConfig;
import ru.deirel.mdl.api.IDisplayManagerConfig;
import ru.deirel.mdl.impl.*;

import starling.display.Sprite;

public class DisplayManagerConfigTest implements IDisplayManagerConfig {
    private var _flashBase:flash.display.Sprite = new flash.display.Sprite();
    private var _flashBg:flash.display.Sprite = new flash.display.Sprite();
    private var _flashFg:flash.display.Sprite = new flash.display.Sprite();

    private var _starlingBase:starling.display.Sprite = new starling.display.Sprite();
    private var _starlingBg:starling.display.Sprite = new starling.display.Sprite();
    private var _starlingFg:starling.display.Sprite = new starling.display.Sprite();

    public function DisplayManagerConfigTest() {
        _flashBase.addChild(_flashBg);
        _flashBase.addChild(_flashFg);

        _starlingBase.addChild(_starlingBg);
        _starlingBase.addChild(_starlingFg);
    }

    public function getContentConfig(type:int):IContentConfig {
        if (type == ContentManagerType.BG) {
            return new ContentConfig(_flashBg, _starlingBg);
        } else if (type == ContentManagerType.FG) {
            return new ContentConfig(_flashFg, _starlingFg);
        }
        return null;
    }

    public function get flashBase():flash.display.Sprite {
        return _flashBase;
    }

    public function get starlingBase():starling.display.Sprite {
        return _starlingBase;
    }
}
}
