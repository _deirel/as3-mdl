/**
 * Created by dev on 15.09.2016.
 */
package test {
import ru.deirel.mdl.api.IDisplayManagerConfig;
import ru.deirel.mdl.impl.ContentManager;

public class DisplayManagerTest {
    private var _backgroundManager:ContentManager;
    private var _foregroundManager:ContentManager;

    public function DisplayManagerTest(config:IDisplayManagerConfig) {
        _backgroundManager = new ContentManager(config.getContentConfig(ContentManagerType.BG));
        _foregroundManager = new ContentManager(config.getContentConfig(ContentManagerType.FG));
    }

    public function get background():ContentManager {
        return _backgroundManager;
    }

    public function get foreground():ContentManager {
        return _foregroundManager;
    }
}
}
